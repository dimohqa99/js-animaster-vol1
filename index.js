(function main() {
  document.getElementById("fadeInPlay").addEventListener("click", function() {
    const block = document.getElementById("fadeInBlock");
    animaster().fadeIn(block, 5000);
  });

  document.getElementById("fadeOutPlay").addEventListener("click", function() {
    const block = document.getElementById("fadeOutBlock");
    animaster().fadeOut(block, 5000);
  });
  document.getElementById("movePlay").addEventListener("click", function() {
    const block = document.getElementById("moveBlock");
    animaster().move(block, 1000, { x: 100, y: 10 });
  });

  document.getElementById("scalePlay").addEventListener("click", function() {
    const block = document.getElementById("scaleBlock");
    animaster().scale(block, 1000, 1.25);
  });
  document
    .getElementById("moveAndHidePlay")
    .addEventListener("click", function() {
      const block = document.getElementById("moveAndHideBlock");
      let startAnimation = animaster().moveAndHide(block, 5000, {
        x: 100,
        y: 20
      });
      document
        .getElementById("moveAndHideReset")
        .addEventListener("click", function() {
          startAnimation.reset(block);
        });
    });
  document
    .getElementById("showAndHidePlay")
    .addEventListener("click", function() {
      const block = document.getElementById("showAndHideBlock");
      animaster().showAndHide(block, 5000);
    });
  document
    .getElementById("heartBeatingPlay")
    .addEventListener("click", function() {
      const block = document.getElementById("heartBeatingBlock");
      startAnimation = animaster().heartBeating(block, 1000);
      document
        .getElementById("heartBeatingStop")
        .addEventListener("click", function() {
          startAnimation.stop();
        });
    });
  document.getElementById("shakingPlay").addEventListener("click", function() {
    const block = document.getElementById("shakingBlock");
    startAnimation = animaster().shaking(block, 500);
    document
      .getElementById("shakingStop")
      .addEventListener("click", function() {
        startAnimation.stop();
      });
  });
})();

function getTransform(translation, ratio) {
  const result = [];
  if (translation) {
    result.push(`translate(${translation.x}px,${translation.y}px)`);
  }
  if (ratio) {
    result.push(`scale(${ratio})`);
  }
  return result.join(" ");
}

function animaster() {
  return {
    fadeIn: function(element, duration) {
      element.style.transitionDuration = `${duration}ms`;
      element.classList.remove("hide");
      element.classList.add("show");
    },
    fadeOut: function(element, duration) {
      element.style.transitionDuration = `${duration}ms`;
      element.classList.remove("show");
      element.classList.add("hide");
    },
    move: function(element, duration, translation) {
      element.style.transitionDuration = `${duration}ms`;
      element.style.transform = getTransform(translation, null);
    },
    scale: function(element, duration, ratio) {
      element.style.transitionDuration = `${duration}ms`;
      element.style.transform = getTransform(null, ratio);
    },

    moveAndHide: function(element, duration, translation) {
      let timerId = setTimeout(() => {
        this.move(element, (duration * 2) / 5, translation);
        timerId = setTimeout(() => {
          this.fadeOut(element, (duration * 3) / 5);
        }, (duration * 2) / 5);
      }, 0);
      return {
        reset: function(element) {
          clearTimeout(timerId);
          resetFadeIn(element);
          resetFadeOut(element);
          resetMoveAndScale(element);
        }
      };
    },

    showAndHide: function(element, duration) {
        this.fadeIn(element, 1 / 3 * duration);
        setTimeout(() => this.fadeOut(element, 1 / 3 * duration), 1 / 3 * duration);
    },

    heartBeating: function(element, duration) {
      let timerId = setInterval(() => {
        this.scale(element, duration * 0.5, 1.4);
        setTimeout(() => {
          this.scale(element, duration * 0.5, 1);
        }, duration * 0.5);
      }, duration);

      return {
        stop: function() {
          clearInterval(timerId);
        }
      };
    },

    shaking: function(element, duration, translation) {
      let timerId = setInterval(() => {
        this.move(element, duration / 2, { x: 20, y: 0 });
        setTimeout(() => {
          this.move(element, duration / 2, { x: 0, y: 0 });
        }, duration / 2);
      }, duration);
      return {
        stop: function() {
          clearInterval(timerId);
        }
      };
    }
  };

  function resetFadeIn(element) {
    element.classList.add("hide");
    element.classList.remove("show");
    element.style.transitionDuration = null;
  }

  function resetFadeOut(element) {
    element.style.transitionDuration = null;
    element.classList.add("show");
    element.classList.remove("hide");
  }

  function resetMoveAndScale(element) {
    element.style.transitionDuration = null;
    element.style.transform = null;
  }
}